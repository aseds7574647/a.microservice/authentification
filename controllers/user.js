const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const User = require('../models/User');

exports.register = async (req, res) => {
    try {
        // Vérifiez si l'email respecte le format d'un email
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!emailRegex.test(req.body.email)) {
            return res.status(400).json({ error: 'Format d\'email invalide' });
        }

        // Vérifiez si le mot de passe a au moins 5 caractères
        if (req.body.password.length < 5) {
            return res.status(400).json({ error: 'Le mot de passe doit avoir au moins 5 caractères' });
        }

        // Hash du mot de passe
        const hashedPassword = await bcrypt.hash(req.body.password, 10);

        // Créez un nouvel utilisateur
        const user = new User({
            email: req.body.email,
            password: hashedPassword
        });

        // Sauvegarde de l'utilisateur dans la base de données
        await user.save();

        // Générez un token JWT pour l'utilisateur
        const token = jwt.sign(
            { userId: user._id },
            'PROJET_MCOMMERCE_ASEDS_INE_2023',
            { expiresIn: '1h' }
        );

        // Retournez une réponse réussie avec le token
        return res.status(201).json({
            message: 'Utilisateur créé !',
            token: token
        });
    } catch (error) {
        console.error('Erreur lors de l\'inscription :', error);
        // Retournez une réponse d'erreur générale
        return res.status(500).json({ error: 'Erreur lors de l\'inscription' });
    }
};

// Endpoint de connexion
exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(401).json({ error: 'Utilisateur non trouvé !' });
            }
            bcrypt.compare(req.body.password, user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'Mot de passe incorrect !' });
                    }
                    res.status(200).json({
                        userId: user._id,
                        token: jwt.sign(
                            { userId: user._id },
                            'PROJET_MCOMMERCE_ASEDS_INE_2023',
                            { expiresIn: '2h' }
                        )
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};