# Utilise l'image Node.js 18-alpine
FROM node:18-alpine

# Définit le répertoire de travail à /Mcommerce
WORKDIR /usr/src/app

# Copie les fichiers du microservice dans le conteneur à /Mcommerce/commande
COPY . /Mcommerce/authentification

# Installe les dépendances du projet
RUN npm install --force

# Expose le port 3001 du conteneur (ou le port que votre microservice utilise)
EXPOSE 3008

# Commande à exécuter lors du démarrage du conteneur
CMD ["node", "serverAuth.js"]
